# Hiro2Hedera #



### What is Hiro2Hedera? ###

**Hiro2Hedera (H2H)** is a wrapped **STX** & **HBAR** token and interoperability protocol 
between the **Stacks 2.0** and **Hedera** Testnets / Mainnets using **Clarity** smart contracts and **HTS**.